﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;

namespace Mule.Data
{
    class Branch_Param : GH_Param<Branch>  //IGH_PreviewObject
    {
        #region FIELD
        //private bool m_hidden = false;
        #endregion

        #region CONSTRUCTOR
        public Branch_Param()
            : base(new GH_InstanceDescription(
                "Branches",
                "B",
                "Connections beween nodes",  //The edge of topology graph
                "Donkey",
                "Mule"),GH_ParamAccess.list)
        { }
        ////constructor 1 overload        
        //public T_Element_Param(GH_InstanceDescription nTag) : base(nTag) { }
        ////constructor 2 overload      
        //public T_Element_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        #endregion

        #region GH_Param
        public override Guid ComponentGuid
        {
            get { return new Guid("{191E863C-32A2-4944-915A-A953780C93D4}"); }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_branch; }
        }
        #endregion
    }
}
