﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;

namespace Mule.Data
{
    public class Branch : Utilities.GH.Goo
    {
        #region FIELD
        public double q { get; private set; }
        public double Force { get; private set; }
        public Curve Crv { get; private set; }
        //public int DivideCount { get; set; }
        public bool IsFixed_q {get; private set;}
        public double Length { get; private set; }
        #endregion

        #region CONSTRUCTOR
        public Branch(Curve crv, double force, bool as_force_density, bool fixed_q)
        {
            this.Crv = crv;
            this.Length = crv.GetLength();
            if (as_force_density)
            {
                this.q = force;
                this.Force = force * this.Length;
            }
            else 
            {
                this.Force = force;
                this.q = force / this.Length;
            }

            this.IsFixed_q = fixed_q;

            //this.DivideCount = -1; //Do we need this? :)
        }

        public Branch()
            : this((Curve)null, 1.0, true, false) { }

        public Branch(Branch element)
            : this(element.Crv, element.q, true, element.IsFixed_q) { }
        #endregion

        #region METHODS
        //public Curve[] Subdivide()
        //{
        //    return this.Subdivide(this.DivideCount);
        //}

        public List<Branch> Subdivide(int count)
        {
            double[] divideParams= this.Crv.DivideByCount(count,false);
            Curve[] segments = this.Crv.Split(divideParams);
            List<Branch> sub_branches = new List<Branch>(segments.Length);
            foreach (Curve crv in segments)
                if(this.IsFixed_q)
                    sub_branches.Add(new Branch(crv, this.q, true, this.IsFixed_q));
                else
                    sub_branches.Add(new Branch(crv, this.Force, false, this.IsFixed_q));
            return sub_branches;
        }

        //public Branch[] Split(Point3d pt)
        //{
        //    double t;
        //    if(!this.Crv.ClosestPoint(pt, out t))
        //        throw new ArgumentException("Point doesn't lies on a curve", pt.ToString());

        //    List<double> param = new List<double>();
        //    param.Add(t);
        //    return this.Split(param);
        //}

        public Branch[] Split(List<double> parms)
        {
            Curve[] segments = this.Crv.Split(parms);
            Branch[] newBranches = new Branch[segments.Length];
            for (int i = 0; i < segments.Length; i++ )
            {
                if(IsFixed_q)
                    newBranches[i] = new Branch(segments[i], this.q, true, true);
                else
                    newBranches[i] = new Branch(segments[i], this.Force, false, false);
            }
            return newBranches;
        }
        #endregion

        #region Goo
        public override IGH_Goo Duplicate()
        {
            return this;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return this.TypeName
               + " q = " + this.q
               + " (" + this.Force + " N)";
        }

        public override string TypeDescription
        {
            get { return "Branch of diagram."; }
        }

        public override string TypeName
        {
            get { return "Branch"; }
        }

        //ADDITIONAL
        public override bool CastFrom(object source)
        {
            if (source == null)
                return false;

            if (source is GH_Curve)
            {
                GH_Curve crv = (GH_Curve)source;
                this.Crv = crv.Value;
                return true;
            }

            return false;
        }

        public override bool CastTo<T>(out T target)
        {
            if (typeof(T).IsAssignableFrom(typeof(GH_Curve)))
            {
                object pt = new GH_Curve(this.Crv);
                target = (T)pt;
                return true;
            }

            if (typeof(T).IsAssignableFrom(typeof(GH_Number)))
            {
                object vec = new GH_Number(this.q);
                target = (T)vec;
                return true;
            }
            return base.CastTo<T>(out target);
        }
        #endregion
    }
}
