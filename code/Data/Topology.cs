﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.GUI.Gradient;
using Mule.Data;
using Rhino.Geometry;
using Mule.Utilities.Preview;
using Grasshopper.GUI.Gradient;
using Grasshopper.Kernel.Types;

namespace Mule.Data
{
    public class Topology : Utilities.GH.Goo
    {
        #region FIELD
        public Dictionary<Point3d, int> Nodes { get; private set; }
        public List<Branch> Branches { get; private set; }
        public List<bool> Fixed { get; private set; }
        public List<Vector3d> Forces { get; private set; }
        //public List<double> ForceDensities { get; private set; }
        //PREVIEW DATA
        public List<Arrow3D> Loads {get; private set;}
        public List<Branch3D> Branches_preview {get; private set;}
        GH_Gradient gradient;
        #endregion

        #region CONSTRUCTOR
        public Topology()
            : this(new List<Branch>()) { }

        public Topology(List<Branch> basic_branches)
        {
        	this.gradient = new GH_Gradient();
            //gradient.AddGrip(-0.05, Color.DodgerBlue);
            //gradient.AddGrip(-2, Color.DodgerBlue);
            //gradient.AddGrip(0, Color.White);
            //gradient.AddGrip(0.05, Color.Crimson); //Crimson
            //gradient.AddGrip(2, Color.Crimson);
            
            this.Branches = basic_branches;
            this.Nodes = new Dictionary<Point3d, int>(new Point3dComparer());  //TODO: class
            //INITIALIZE LIST OF NODES
            this.InitializeNodes(this.Branches);

            //INITIALIZE LIST OF FREE NODES
            this.Fixed = new List<bool>(this.Nodes.Count);
            foreach (var item in this.Nodes)
                Fixed.Add(false);

            //INITIALIZE LIST OF ZERO LOADS
            this.Forces = new List<Vector3d>(this.Nodes.Count);
            foreach (var item in this.Nodes)
                Forces.Add(Vector3d.Zero);
            
            //INITIALIZE PREVIEW DATA
            this.Loads = null;
        }
        #endregion

        #region METHODS
        //PUBLIC
        public void SetNodeData(List<Point3d> fixed_points, List<PointLoad> point_loads)
        {
            //TODO split geometry by point
            this.AddFixedNodes(fixed_points);
            this.AddPointLoads(point_loads);
            //Final geometry - ELEMENTS (store)
        }

        public bool SubdivideBranches(int divideCount)
        {
            List<Branch> sub_branches = new List<Branch>(this.Branches.Count);

            foreach (Branch branch in this.Branches)
            {
                List<Branch> segments;
                //if (branch.DivideCount == -1)
                    segments = branch.Subdivide(divideCount);  // Global divide
                //else
                   // segments = branch.Subdivide();  // Custom divide

                //ADD DIVIDED NODES (only endpoints of each new segment exept last one - already exists)
                for (int i = 0; i < segments.Count - 1; i++)
                {
                    this.AddNode(segments[i].Crv.PointAtEnd, false);
                    this.Fixed.Add(false);
                    // force synchronization if needed..
                }
                sub_branches.AddRange(segments);
            }
            this.Branches = sub_branches;
            this.SychronizeForces(); //TODO + fixed points - or custom class :)
            return true;
        }

        public void AddGravitation(double gravitation_value)  // Using physical lengths of branches
        {
            foreach (Branch branch in this.Branches)
            {
                double l = branch.Length;
                double deadLoad = (l *0.5)* gravitation_value;
                Vector3d point_deadLoad = new Vector3d(0, 0, deadLoad);
                this.Forces[this.Nodes[branch.Crv.PointAtStart]] += point_deadLoad;
                this.Forces[this.Nodes[branch.Crv.PointAtEnd]] += point_deadLoad;
            }
        }

        //public void Calculate_PreviewData()
        //{

        //}
        
        //PRIVATE
        private void InitializeNodes(List<Branch> branches)
        {
            foreach(Branch branch in branches)
            {
                this.AddNode(branch.Crv.PointAtStart, true);
                this.AddNode(branch.Crv.PointAtEnd, true);
            }
        }

        private bool AddNode(Point3d pt, bool checkDuplicates)
        {
            if (checkDuplicates)
                if (this.Nodes.ContainsKey(pt))
                    return false;

            int count = this.Nodes.Count;
            this.Nodes.Add(pt, count);
            return true;
        }

        private void AddFixedNodes(List<Point3d> fix_pts)
        {
            //SET FIXED POINT TO TRUE VALUE
            foreach (Point3d pt in fix_pts)
            {
                if(this.Nodes.ContainsKey(pt))
                    this.Fixed[this.Nodes[pt]] = true;
                else
                    throw new ArgumentException("Fixed point doesn't lie on any branch!"
                        , pt.ToString());
            }
        }

        private void AddPointLoads(IList<PointLoad> point_loads)
        {
            //SET FORCES TO NODES
            for (int i = 0; i < point_loads.Count; i++)
            {
                if (this.Nodes.ContainsKey(point_loads[i].Position))
                    this.Forces[this.Nodes[point_loads[i].Position]] = (Vector3d)point_loads[i].Force; 
                else
                    throw new ArgumentException("Point load doesn't lie on any branch!"
                        , point_loads[i].Position.ToString());
            }
        }

        private void SychronizeForces()  // Add blank zero forces to sychronize forces with nodes count
        {
            int basic_loads = this.Forces.Count;
            for (int i = basic_loads; i < this.Nodes.Count; i++)
            {
                this.Forces.Add(Vector3d.Zero);
            }
        }

        public int GetNode_ID(Point3d pt)
        {
            if (this.Nodes.ContainsKey(pt))
                return this.Nodes[pt];
            else
                throw new ArgumentException("Node does not exist in the topology!");
        }
        #endregion
        
        #region PREVIEW DATA
        public void GeneratePreview()
        {
            //CALCULATE GRADIENT
            double min_force = Double.MaxValue;
            double max_force = Double.MinValue;

            foreach (Branch branch in Branches)
            {
                if (branch.Force < min_force)
                    min_force = branch.Force;
                if (branch.Force > max_force)
                    max_force = branch.Force;
            }

            double max_value;
            double abs_minForce = Math.Abs(min_force);
            if (abs_minForce > max_force)
                max_value = abs_minForce;
            else
                max_value = max_force;

            double brightness = 0.5;
            Color blue = Color.DodgerBlue;
            Color blue_dark = Color.FromArgb(blue.A, (int)(blue.R * brightness), (int)(blue.G * brightness), (int)(blue.B * brightness));
            Color red = Color.Red;
            brightness = 0.75;
            Color red_dark = Color.FromArgb(red.A, (int)(red.R * brightness), (int)(red.G * brightness), (int)(red.B * brightness));
            //brightness = 1.25;
            //Color red_light = Color.FromArgb(red.A, (int)(red.R * brightness), (int)(red.G * brightness), (int)(red.B * brightness));

            this.gradient = new GH_Gradient();
            this.gradient.AddGrip(-max_value, blue_dark);
            this.gradient.AddGrip(-0.001, Color.DodgerBlue);
            //gradient.AddGrip(0, Color.White);
            this.gradient.AddGrip(0.001, red); //Crimson
            this.gradient.AddGrip(max_value, red_dark);

        	this.Loads = new List<Arrow3D>();
        	var points = this.Nodes.Keys.ToArray<Point3d>();
    		int i = -1;
        	foreach(Vector3d force in this.Forces)
        	{
        		i++;
        		if(force.IsZero) continue;
        		this.Loads.Add(new Arrow3D(new Line(points[i]+(-force),force)));
        	}

            int max_thikness = 12;
        	this.Branches_preview = new List<Branch3D>();
        	foreach (Branch branch in this.Branches)
            {
                double q = branch.Force;
                Color color = this.gradient.ColourAt(q); //TODO topology gradient min/max value
                //if( q < 0)
                //    color = Color.Red;
                //else
                //    color = Color.SkyBlue;

                //double scale = Math.Abs(q)/2;
                int thickness = 1 + (int)Math.Round(max_thikness * (Math.Abs(branch.Force) / max_value)); //(int)(1+ (5 * scale));
                
                Branches_preview.Add(new Branch3D(new Line(branch.Crv.PointAtStart,branch.Crv.PointAtEnd), color, thickness));
            }
        }
        #endregion

        #region Goo
        public override IGH_Goo Duplicate()
        {
            return this;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return this.TypeName
               + " [" + this.Branches.Count
               + "x" + this.Nodes.Count +"]";
        }

        public override string TypeDescription
        {
            get { return "Topology."; }
        }

        public override string TypeName
        {
            get { return "Topology"; }
        }
        #endregion
    }

    //class ConnectionComparer : EqualityComparer<Curve>
    //{
    //    /// <summary>
    //    /// Compare topology branches
    //    /// </summary>
    //    /// <param name="crv1">First branch to compare</param>
    //    /// <param name="crv2">Second branch to compare</param>
    //    /// <returns>Return true if branch are equal or different oriented</returns>
    //    public override bool Equals(Curve crv1, Curve crv2)
    //    {
    //        return (EqualsPoints(crv1.PointAtStart, crv2.PointAtStart))
    //            && (EqualsPoints(crv1.PointAtEnd, crv2.PointAtEnd))
    //            || (EqualsPoints(crv1.PointAtStart, crv2.PointAtEnd))  //OR differen torientation
    //            && (EqualsPoints(crv1.PointAtEnd, crv2.PointAtStart));

    //        //HACK crv - is linear, orientation, etc.
    //    }

    //    //GENERATE HASHCODE
    //    public override int GetHashCode(Curve crv)
    //    {
    //        return crv.GetHashCode();
    //    }

    //    private bool EqualsPoints(Point3d pt1, Point3d pt2)
    //    {
    //        int round = Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance.ToString().Length - 2; //TODO check it!
    //        return (Math.Round(pt1.X, round) == Math.Round(pt2.X, round))
    //            && (Math.Round(pt1.Y, round) == Math.Round(pt2.Y, round))
    //            && (Math.Round(pt1.Z, round) == Math.Round(pt2.Z, round));
    //    }
    //}
}
