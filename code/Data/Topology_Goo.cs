﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel.Types;

namespace Mule.Data
{
    class Topology_Goo : GH_Goo<Topology>//: Utilities.GH.Goo
    {
        public Topology_Goo()
            : this(new Topology()) { }

        public Topology_Goo(Topology value)
        {
            this.Value = value;
        }
        //#region Goo
        //public override IGH_Goo Duplicate()
        //{
        //    return this;
        //}

        //public override bool IsValid
        //{
        //    get { return true; }
        //}

        //public override string ToString()
        //{
        //    return this.TypeName
        //       + " q = " + this.q
        //       + " (" + this.Force + " N)";
        //}

        //public override string TypeDescription
        //{
        //    get { return "Branch of diagram."; }
        //}

        //public override string TypeName
        //{
        //    get { return "Branch"; }
        //}

        ////ADDITIONAL
        //public override bool CastFrom(object source)
        //{
        //    if (source == null)
        //        return false;

        //    if (source is GH_Curve)
        //    {
        //        GH_Curve crv = (GH_Curve)source;
        //        this.Crv = crv.Value;
        //        return true;
        //    }

        //    return false;
        //}

        //public override bool CastTo<T>(out T target)
        //{
        //    if (typeof(T).IsAssignableFrom(typeof(GH_Curve)))
        //    {
        //        object pt = new GH_Curve(this.Crv);
        //        target = (T)pt;
        //        return true;
        //    }

        //    if (typeof(T).IsAssignableFrom(typeof(GH_Number)))
        //    {
        //        object vec = new GH_Number(this.q);
        //        target = (T)vec;
        //        return true;
        //    }
        //    return base.CastTo<T>(out target);
        //}
        //#endregion
        public override IGH_Goo Duplicate()
        {
            return this;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return this.TypeName
               + " [" + this.Value.Branches.Count
               + "x" + this.Value.Nodes.Count + "]";
        }

        public override string TypeDescription
        {
            get { return "Topology."; }
        }

        public override string TypeName
        {
            get { return "Topology"; }
        }
    }
}
