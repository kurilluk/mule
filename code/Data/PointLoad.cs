﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GH_IO.Types;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;

namespace Mule.Data
{
    public class PointLoad : Utilities.GH.Goo
    {
        public Vector3d Force { get; private set; }
        public Point3d Position { get; private set; }

        public PointLoad()
            : this(Point3d.Unset, Vector3d.Unset) { }

        public PointLoad(PointLoad element)
            : this(element.Position, element.Force) { }

        public PointLoad(Point3d position, Vector3d force)
        {
            this.Position = position;
            this.Force = force;
        }

        public double Magnitude
        {
            get { return this.Force.Length; }
        }

        //#region IGH_Goo
        //public bool CastFrom(object source)
        //{
        //    if (source == null)
        //        return false;

        //    if (source is GH_Point)
        //    {
        //        GH_Point pt = (GH_Point)source;
        //        this.Force = new Vector3d(0, 0, 10);
        //        this.Position = new Point3d(pt.Value.X, pt.Value.X, pt.Value.Z);
        //        return true;
        //    }

        //    return false;
        //}

        //public bool CastTo<T>(out T target)
        //{
        //    if (typeof(T).IsAssignableFrom(typeof(GH_Point)))
        //    {
        //        object pt = new GH_Point(this.Position);
        //        target = (T)pt;
        //        return true;
        //    }

        //    if (typeof(T).IsAssignableFrom(typeof(GH_Vector)))
        //    {
        //        object vec = new GH_Vector(this.Force);
        //        target = (T)vec;
        //        return true;
        //    }

        //    target = default(T);
        //    return false;
        //}

        //public IGH_Goo Duplicate()
        //{
        //    return new PointLoad(this);
        //}

        //public IGH_GooProxy EmitProxy()
        //{
        //    return (IGH_GooProxy)null;
        //}

        //public bool IsValid
        //{
        //    get { return true; }
        //}

        //public string IsValidWhyNot
        //{
        //    get { return "Pokazil sa.. nie je to co byval."; }
        //}

        //public object ScriptVariable()
        //{
        //    return (object)this;
        //}

        //public string TypeDescription
        //{
        //    get { return "Point load"; }
        //}

        //public string TypeName
        //{
        //    get { return "PointLoad"; }
        //}

        //public bool Read(GH_IO.Serialization.GH_IReader reader)
        //{
        //    GH_Point3D pt = reader.GetPoint3D("point_load.position");
        //    this.Position = new Point3d(pt.x, pt.y, pt.z);
        //    GH_Point3D vec = reader.GetPoint3D("point_load.force");
        //    this.Force = new Vector3d(vec.x, vec.y, vec.z);
        //    return true;
        //}

        //public bool Write(GH_IO.Serialization.GH_IWriter writer)
        //{
        //    writer.SetPoint3D("point_load.position", new GH_Point3D(this.Position.X, this.Position.Y, this.Position.Z));
        //    writer.SetPoint3D("point_load.force", new GH_Point3D(this.Force.X, this.Force.Y, this.Force.Z));
        //    return true;
        //}

        //public override string ToString()
        //{
        //    return this.TypeName
        //        + ": " + this.Magnitude + "[N]";
        //}
        //#endregion

        #region Goo
        public override bool IsValid
        {
            get { return true; }
        }

        public override string TypeName
        {
            get { return "PointLoad"; }
        }

        public override string TypeDescription
        {
            get { return "Point load"; }
        }

        public override IGH_Goo Duplicate()
        {
            return new PointLoad(this);
        }

        public override string ToString()
        {
            return this.TypeName
                + ": " + this.Magnitude + "[N]";
        }

        public override bool CastFrom(object source)
        {
            if (source == null)
                return false;

            if (source is GH_Point)
            {
                GH_Point pt = (GH_Point)source;
                this.Force = new Vector3d(0, 0, 10);
                this.Position = new Point3d(pt.Value.X, pt.Value.X, pt.Value.Z);
                return true;
            }

            return false;
        }

        public override bool CastTo<T>(out T target)
        {
            if (typeof(T).IsAssignableFrom(typeof(GH_Point)))
            {
                object pt = new GH_Point(this.Position);
                target = (T)pt;
                return true;
            }

            if (typeof(T).IsAssignableFrom(typeof(GH_Vector)))
            {
                object vec = new GH_Vector(this.Force);
                target = (T)vec;
                return true;
            }
            return base.CastTo<T>(out target);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            GH_Point3D pt = reader.GetPoint3D("point_load.position");
            this.Position = new Point3d(pt.x, pt.y, pt.z);
            GH_Point3D vec = reader.GetPoint3D("point_load.force");
            this.Force = new Vector3d(vec.x, vec.y, vec.z);
            return true;
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetPoint3D("point_load.position", new GH_Point3D(this.Position.X, this.Position.Y, this.Position.Z));
            writer.SetPoint3D("point_load.force", new GH_Point3D(this.Force.X, this.Force.Y, this.Force.Z));
            return true;
        }
        #endregion
    }
}
