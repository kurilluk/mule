﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;

namespace Mule.Data
{
    class Topology_Param : GH_Param<Topology>
    {
        #region FIELD
        //private bool m_hidden = false;
        private GH_ParamAccess m_access;
        #endregion

        #region CONSTRUCTOR
        public Topology_Param()
            : base(new GH_InstanceDescription(
                "Topology",
                "T",
                "Topology",
                "Donkey",
                "Mule"),GH_ParamAccess.list)
        {
            this.m_access = GH_ParamAccess.item;
        }
        ////constructor 1 overload        
        //public T_Element_Param(GH_InstanceDescription nTag) : base(nTag) { }
        ////constructor 2 overload      
        //public T_Element_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        #endregion

        #region GH_Param
        public override Guid ComponentGuid
        {
            get { return new Guid("{C438CAC6-D530-4DD7-A954-FFDED2FC85F0}"); }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_topology; }
        }

        public override GH_ParamAccess Access
        {
            get
            {
                return this.m_access;
            }
            set
            {
                this.m_access = value;
            }
        }
        #endregion
    }
}
