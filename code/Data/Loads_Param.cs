﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;

namespace Mule.Data
{
    class Loads_Param : GH_Param<PointLoad>  //IGH_PreviewObject
    {
        #region FIELD
        //private bool m_hidden = false;
        #endregion

        #region CONSTRUCTOR
        public Loads_Param()
            : base(new GH_InstanceDescription(
                "Loads",
                "p",
                "Point load",
                "Donkey",
                "Mule"),GH_ParamAccess.list)
        { }
        ////constructor 1 overload        
        //public T_Element_Param(GH_InstanceDescription nTag) : base(nTag) { }
        ////constructor 2 overload      
        //public T_Element_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        #endregion

        #region GH_Param
        public override Guid ComponentGuid
        {
            get { return new Guid("{79D386A1-11FE-4AC7-9010-CE8FAC1DA57E}"); }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_load_nodal; }
        }
        #endregion
    }
}
