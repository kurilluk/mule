﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Geometry;

namespace Mule.Data
{
    public class X_Topology
    {
        #region FIELD
        public HashSet<Point3d> Nodes { get; private set; }
        public HashSet<Curve> Branches { get; private set; }
        #endregion

        #region CONSTRUCTOR
        public X_Topology()
        {
            this.Branches = new HashSet<Curve>(new ConnectionComparer());
            this.Nodes = new HashSet<Point3d>(new Point3dComparer());

            Point3d pt = Nodes.ElementAt(0);
        }
        #endregion

        #region METHODS
        public bool AddBranch(Curve branch)
        {
            if (this.Branches.Contains(branch))
                return false;

            return true;
        }
        #endregion
    }

    class Point3dComparer : EqualityComparer<Point3d>
    {
        /// <summary>
        /// Compare Rhino/Grasshopper 3d points with tolerance
        /// </summary>
        /// <param name="pt1">First point to compare</param>
        /// <param name="pt2">Second point to compare</param>
        /// <returns>Return true if coordinates are equal</returns>
        public override bool Equals(Point3d pt1, Point3d pt2)
        {
            return EqualsPoints(pt1, pt2); ;
        }

        static public bool EqualsPoints(Point3d pt1, Point3d pt2)
        {
            int round = Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance.ToString().Length - 2;
            return (Math.Round(pt1.X, round) == Math.Round(pt2.X, round))
                && (Math.Round(pt1.Y, round) == Math.Round(pt2.Y, round))
                && (Math.Round(pt1.Z, round) == Math.Round(pt2.Z, round));
        }

        //GENERATE HASHCODE
        public override int GetHashCode(Point3d pt)
        {
            return ((float)pt.X + ";" + (float)pt.Y + ";" + (float)pt.Z).GetHashCode();

            //return (pt.X + ";" + pt.Y + ";" + pt.Z).GetHashCode();

             //return (Math.Round(pt.X, 1).ToString() + ";"
             //    + Math.Round(pt.Y, 1).ToString() + ";"
             //    + Math.Round(pt.Z, 1).ToString()).GetHashCode();

            //return pt.X.GetHashCode() ^ pt.Y.GetHashCode() ^ pt.Z.GetHashCode();
            //return pt.GetHashCode();
        }
    }

    class ConnectionComparer : EqualityComparer<Curve>
    {
        /// <summary>
        /// Compare topology branches
        /// </summary>
        /// <param name="crv1">First branch to compare</param>
        /// <param name="crv2">Second branch to compare</param>
        /// <returns>Return true if branch are equal or different oriented</returns>
        public override bool Equals(Curve crv1, Curve crv2)
        {
            return (Point3dComparer.EqualsPoints(crv1.PointAtStart, crv2.PointAtStart))
                && (Point3dComparer.EqualsPoints(crv1.PointAtEnd, crv2.PointAtEnd))
                || (Point3dComparer.EqualsPoints(crv1.PointAtStart, crv2.PointAtEnd))  //OR differen torientation
                && (Point3dComparer.EqualsPoints(crv1.PointAtEnd, crv2.PointAtStart));

            //HACK crv - is linear, orientation, etc.
        }

        //GENERATE HASHCODE
        public override int GetHashCode(Curve crv)
        {
            return crv.GetHashCode();
        }
    }
}
