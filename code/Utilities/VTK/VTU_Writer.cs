﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Rhino.Geometry;
using System.Collections;
using Mule.Data;

namespace Mule.Utilities.VTK
{
    public class VTU_Writer
    {
        #region FIELD
        private XmlWriter m_doc;
        private StringBuilder m_connectivity, m_offsets, m_types;
        private StringBuilder m_force_density;
        private StringBuilder m_pointLoad, m_support;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public VTU_Writer() { }
        #endregion

        #region IFile_Writer Members
        //MAIN WRITE METHOD
        public void Write(Topology topology, string filePath)
        {
            m_doc = XmlWriter.Create(filePath, Settings());
            m_doc.WriteStartDocument();
            //VTK FILE (Open)
            m_doc.WriteStartElement("VTKFile");
            m_doc.WriteAttributeString("type", "UnstructuredGrid");
            m_doc.WriteAttributeString("version", "0.1");
            m_doc.WriteAttributeString("byte_order", "LittleEndian");
            //UNSTRUCTURED GRID (Open)
            m_doc.WriteStartElement("UnstructuredGrid");
            //PIECE (Open)
            m_doc.WriteStartElement("Piece");
            m_doc.WriteAttributeString("NumberOfPoints", topology.Nodes.Count.ToString());
            m_doc.WriteAttributeString("NumberOfCells", topology.Branches.Count.ToString());

            //POINTS
            m_doc.WriteStartElement("Points");
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("type", "Float32");
            m_doc.WriteAttributeString("NumberOfComponents", "3");
            m_doc.WriteAttributeString("format", "ascii");
            WritePoints(topology.Nodes.Keys.ToArray<Point3d>()); //TODO have a look on it!!!
            m_doc.WriteEndElement();
            m_doc.WriteEndElement();

            //CELLS
            InitCells(topology);
            m_doc.WriteStartElement("Cells");
            WriteDataArray("Int32", "connectivity", "ascii", m_connectivity);
            WriteDataArray("Int32", "offsets", "ascii", m_offsets);
            WriteDataArray("UInt8", "types", "ascii", m_types);
            m_doc.WriteEndElement();

            //POINT DATA
            InitPointData(topology.Fixed);
            InitPointData(topology.Forces);
            m_doc.WriteStartElement("PointData");
            //    //UNUSED m_doc.WriteAttributeString("Scalars", "Boundary_Conditions");
            //WriteDataArray("ID_Boundary_Condition", m_pointLoad);
            WriteDataArray("PointLoads", m_pointLoad, "Float32", 3);
            WriteDataArray("FixedPoints", m_support); //NODAL DOF
            m_doc.WriteEndElement();

            //CELL DATA
            m_doc.WriteStartElement("CellData");
            //m_force_density = InitCellData(topology.ForceDensities);
                //UNUSED m_doc.WriteAttributeString("Tensors", "Rstress");
            WriteDataArray("ForceDencity", m_force_density,"Float32");
            m_doc.WriteEndElement();

            //PIECE (Close)
            m_doc.WriteEndElement();
            //UNSTRUCTURED GRID (Close)
            m_doc.WriteEndElement();

            //APPENDED DATA (Open)
            //m_doc.WriteStartElement("AppendedData");
            //m_doc.WriteRaw("_");
            // //CHARAKTERISTICS (Open)
            //m_doc.WriteStartElement("Characteristics");
            //ADD COMMENT HERE(?)
            //m_doc.WriteStartElement("PROBLEM_TYPE_DOF");
            //m_doc.WriteAttributeString("Number", "1");
            //m_doc.WriteStartElement("item");
            //m_doc.WriteString("3dRot");
            //m_doc.WriteEndElement();
            //m_doc.WriteEndElement();
            //NODAL DOF - SUPPORT
            //m_doc.WriteRaw(Environment.NewLine);
            //m_doc.WriteStartElement("LIST_PRESCRIBED_VALUES");
            //m_doc.WriteAttributeString("Number", model.Supports.Count.ToString());
            //WriteAppendedDataItems(model.Supports);
            //m_doc.WriteEndElement();
            //CROSS SECTION
            //m_doc.WriteRaw(Environment.NewLine);
            //m_doc.WriteStartElement("LIST_CROSS-SECTIONS");
            // //model.Profiles.RemoveAt(0);
            //m_doc.WriteAttributeString("Number", model.Profiles.Count.ToString());
            //WriteAppendedDataItems(model.Profiles);
            //m_doc.WriteEndElement();
            //MATERIALS
            //m_doc.WriteRaw(Environment.NewLine);
            //m_doc.WriteStartElement("LIST_MATERIALS");
            // //model.Materials.RemoveAt(0);
            //m_doc.WriteAttributeString("Number", model.Materials.Count.ToString());
            //WriteAppendedDataItems(model.Materials);
            // //m_doc.WriteRaw(Environment.NewLine);
            //m_doc.WriteEndElement();
            //BOUNDARY CONDITIONS
            //m_doc.WriteRaw(Environment.NewLine);
            //m_doc.WriteStartElement("LIST_BOUNDARY_CONDITIONS");
            //m_doc.WriteAttributeString("Number", model.Loads.Count.ToString());
            //WriteAppendedDataItems(model.Loads);
            //m_doc.WriteEndElement();

            //VTK FILE (Close)
            m_doc.WriteEndDocument();
            m_doc.Close();
        }
        #endregion

        #region INITIALIZE METHODS
        //INITIALIZE CELLS AND CELL DATA BUFFERS (ELEMENT/GEOMETRY)
        private void InitCells(Topology topology)
        {
            //FIX move to the constructor?
            m_connectivity = new StringBuilder();
            m_offsets = new StringBuilder();
            m_types = new StringBuilder();
            //================================
            m_force_density = new StringBuilder();

            int offsets_Count = 0;

            foreach (Branch branch in topology.Branches)
            {
                m_connectivity.Append(topology.Nodes[branch.Crv.PointAtStart] + " ");
                m_connectivity.Append(topology.Nodes[branch.Crv.PointAtEnd] + " ");
                offsets_Count+=2;

                m_offsets.Append(offsets_Count + " ");
                m_types.Append(3 + " ");

                m_force_density.Append(branch.q + " ");
            }
        }

        //CELL DATA
        //private StringBuilder InitCellData(List<double> forceDensites)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    //================================
        //    foreach (double q in forceDensites)
        //        sb.Append(q + " ");

        //    return sb;
        //}

        //INIT POINT DATA (NODAL LOAD, SUPPORT, HINGE)
        private void InitPointData(List<bool> fixed_points)
        {
            m_support = new StringBuilder();
            //FOR EACH NODE IN MODEL READ DATA
            foreach (bool fixed_pt in fixed_points)
            {
                int support = fixed_pt ? 1 : 0;
                m_support.Append(support+ " ");
            }
        }

        private void InitPointData(List<Vector3d> forces)
        {
            m_pointLoad = new StringBuilder();
            //FOR EACH NODE IN MODEL READ DATA
            foreach (Vector3d force in forces)
            {
                m_pointLoad.Append(force.X + " " + force.Y + " " + force.Z + " ");
            }
        }

        //XML WRITER SETTINGS
        private XmlWriterSettings Settings()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.NewLineChars = "\n"; //\r
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.Encoding = new UTF8Encoding(false);
            return settings;
        }
        #endregion

        #region WRITE METHODS
        //WRITE POINTS
        private void WritePoints(ICollection nodes)
        {
            foreach (Point3d pt in nodes) //TODO improve it!!!
            {
                m_doc.WriteString( pt.X + " " + pt.Y + " " + pt.Z + " ");
            }
        }    
        //WRITE APPENDED DATA ITEMS
        //private void WriteAppendedDataItems<T>(List<T> list) //where T: IAppendedData
        //{
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        m_doc.WriteRaw(Environment.NewLine + m_doc.Settings.IndentChars.ToString());
        //        m_doc.WriteStartElement("item");
        //        m_doc.WriteString( (i+1) + " " + list[i].ToFile());
        //        m_doc.WriteEndElement();
        //    }
        //}
        //WRITE DATA ARRAY(S)
        private void WriteDataArray(string type, string Name, string format, StringBuilder value)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("type", type);
            m_doc.WriteAttributeString("format", format);
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        private void WriteDataArray(string Name, StringBuilder value)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("type", "Int32");
            m_doc.WriteAttributeString("format", "ascii");
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        private void WriteDataArray(string Name, StringBuilder value, string type)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("type", type);
            m_doc.WriteAttributeString("format", "ascii");
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        private void WriteDataArray(string Name, StringBuilder value, string type, int NumberOfComponents)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("type", type);
            m_doc.WriteAttributeString("NumberOfComponents", NumberOfComponents.ToString());
            m_doc.WriteAttributeString("format", "ascii");
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        #endregion
    }
}
