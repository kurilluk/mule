﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;

namespace Mule.Utilities.VTK
{
    public class ReaderBuffer
    {
        //POINTS
        public List<Point3d> Points { get; set; }
        //POINTS DATA
        public List<Vector3d> Displacement { get; set; }
        public List<Vector3d> ReactionForce { get; set; }
        //CELLS
        public List<int> Connectivity { get; set; }
        public List<int> Offsets { get; set; }
        public List<int> Types { get; set; }
        //CELL DATA
        public List<int> Element_ID { get; set; }
        public List<int> Geometry_ID { get; set; }
        public List<double> GValues { get; set; }
        public List<double> BeamStress_N { get; set; }
        public List<double> BeamStress_V { get; set; }
        public List<double> BeamStress_M { get; set; }
        public List<Vector3d> BeamStress_M1_xyz { get; set; }
        public List<Vector3d> BeamStress_M2_xyz { get; set; }
        //MODEL DATA
        public double Stability { get; set; }
        public double Volume { get; set; }

        //public ReaderBuffer()
        //{ this.Stability = 0; }
    }
}
