﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mule.Utilities.VTK
{
    public enum Types
    {
        DUMMY = 0,
        VTK_LINE = 3,
        VTK_POLY_LINE = 4,
        VTK_TRIANGLE = 5,
        VTK_POLYGON = 7,
        VTK_QUAD = 9
    }
}
