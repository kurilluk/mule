﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Rhino.Geometry;

namespace Mule.Utilities.VTK
{
    public class VTU_Reader
    {
        #region FIELDS
        private XmlReader doc;
        public List<Point3d> Points { get; set; }

        public List<int> Connectivity { get; set; }
        public List<int> Offsets { get; set; }
        public List<int> Types { get; set; }

        private List<Line> lines;
        #endregion

        #region CONSTRUCTOR
        public VTU_Reader()
        {
            this.Points = new List<Point3d>();
            this.lines = new List<Line>();
        }
        #endregion

        #region MAIN METHODS
        public List<Line> Read(string FilePath)
        {
            //READ FILE TO BUFFER (Points,Cells, PointData, CellData)
            this.doc = XmlReader.Create(FilePath);
            //ReaderBuffer rb = new ReaderBuffer();
            if (ReadPoints() 
                && ReadCells())
                //&& ReadPointData(ref rb) 
                //&& ReadCellData(ref rb)) 
                //&& ReadAppendedData(ref rb))
            {
                //WRITE BUFFER TO MODEL

                //WriteNodes
                //model.SetNodes(rb.Points);
                //WriteGeometries
                WriteCells();
                //Write Elements
                //WriteElements(rb, ref model);
                //WriteResultingData
                //if (rb.Displacement != null)
                //{
                //    model.Displacement.SetVectors(rb.Displacement);
                //    model.Reactions.SetReactions(rb.ReactionForce);
                //    model.Stress.BeamStress_N = rb.BeamStress_N;
                //    //model.Stress.BeamStress_V = rb.BeamStress_V;
                //    //model.Stress.BeamStress_M = rb.BeamStress_M;
                //    model.Stress.BeamStress_M1_xyz = rb.BeamStress_M1_xyz;
                //    model.Stress.BeamStress_M2_xyz = rb.BeamStress_M2_xyz;
                //    model.GValues = rb.GValues;
                //    //DATA INITIALIZATION
                //    //model.Stress.CalculateAverageNValue(); //HACK TODO VALUE NO N-VALUE
                //    //model appedned data
                //    model.Stability = rb.Stability;
                //    model.Volume = rb.Volume;
                //    model.InitData();
                //}
                //END
                this.doc.Close();
                return this.lines;
            }
            return null;
            ////ReadFile
            //if (ReadNodes() && ReadGeometries())
            //{
            //   if (ReadPointData())
            //   {
            //        return model;
            //   }
            //}
        }
        #endregion
        #region READ FILE
        //READ POINTS
        private bool ReadPoints()
        {
            if (doc.ReadToFollowing("Points"))
            {
                if (doc.ReadToFollowing("DataArray"))
                {
                    List<double> pos = ReadArrDouble(doc.ReadString());
                    for (int i = 0; i < pos.Count; i += 3)
                    {
                        this.Points.Add(new Point3d(pos[i], pos[i + 1], pos[i + 2]));
                    }
                    return true;
                }
            }
            return false;
        }
        //READ CELLS
        private bool ReadCells()
        {
            if (doc.ReadToFollowing("Cells"))
            {
                XmlReader cells = doc.ReadSubtree();
                while (cells.Read())
                {
                    if (cells.HasAttributes && cells.Name == "DataArray")
                    {
                        switch (cells.GetAttribute("Name", cells.NamespaceURI))
                        {
                            case "connectivity":
                                this.Connectivity = ReadArrInteger(doc.ReadString());
                                break;
                            case "offsets":
                                this.Offsets = ReadArrInteger(doc.ReadString());
                                break;
                            case "types":
                                this.Types = ReadArrInteger(doc.ReadString());
                                break;
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        
        //READ POINT DATA - unused
        private bool ReadPointData(ref ReaderBuffer buffer)
        {
            if (doc.ReadToFollowing("PointData"))
            {
                XmlReader pointData = doc.ReadSubtree();
                while (pointData.Read())
                {
                    if (pointData.HasAttributes && pointData.Name == "DataArray")
                    {
                        switch (pointData.GetAttribute("Name", pointData.NamespaceURI))
                        {
                            case "uknw_displacement":
                                List<Vector3d> vectors = ReadArrVector(doc.ReadString());
                                buffer.Displacement = vectors;
                                break;
                            case "uknw_rotation":
                                //read rotation..                               
                                break;
                            case "reactions_forces":
                                List<Vector3d> reactions = ReadArrVector(doc.ReadString());
                                buffer.ReactionForce = reactions;
                                break;
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        
        //READ CELL DATA - unused
        private bool ReadCellData(ref ReaderBuffer cell)
        {
            if (doc.ReadToFollowing("CellData"))
            {
                XmlReader pointData = doc.ReadSubtree();
                while (pointData.Read())
                {
                    if (pointData.HasAttributes && pointData.Name == "DataArray")
                    {
                        switch (pointData.GetAttribute("Name", pointData.NamespaceURI))
                        {
                            case "Property":
                                cell.Element_ID = ReadArrInteger(doc.ReadString());
                                break;
                            case "ID_model_parent":
                                cell.Geometry_ID = ReadArrInteger(doc.ReadString());
                                break;
                            case "stress_beam3D_NVM":
                                List<Vector3d> vectors_NVM = ReadArrVector(doc.ReadString());
                                cell.BeamStress_N = new List<double>(vectors_NVM.Count);
                                cell.BeamStress_V = new List<double>(vectors_NVM.Count);
                                cell.BeamStress_M = new List<double>(vectors_NVM.Count);
                                foreach (Vector3d vector in vectors_NVM)
                                {
                                    cell.BeamStress_N.Add(Math.Round(vector.X, 3));
                                    cell.BeamStress_V.Add(vector.Y);
                                    cell.BeamStress_M.Add(vector.Z);
                                }
                                break;
                            case "stress_beam3D_1_MxMyMz":
                                List<Vector3d> vectors_M1 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_M1_xyz = vectors_M1;
                                break;
                            case "stress_beam3D_2_MxMyMz":
                                List<Vector3d> vectors_M2 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_M2_xyz = vectors_M2;
                                break;
                            case "CSusage_elast_rel":
                                cell.GValues = ReadArrDouble(doc.ReadString());
                                break;
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        
        //READ MODEL DATA - unused
        private bool ReadAppendedData(ref ReaderBuffer buffer)
        {
            if (doc.ReadToFollowing("AppendedData"))
            {
                XmlReader cells = doc.ReadSubtree();
                while (cells.Read())
                {
                    switch (cells.Name)
                    {
                        case "Volume":
                            buffer.Volume = Double.Parse(doc.ReadString());
                            break;
                        case "Stability_critical_coefficient":
                            buffer.Stability = Double.Parse(doc.ReadString());
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
            return false;
        }
        #endregion

        #region WRITE TO MODEL
        //WRITE GEOMETRIES
        private bool WriteCells()
        {
            int a = 0;
            //FOR EACH GEOMETRY
            for (int o = 0; o < this.Offsets.Count; o++)
            {
                //READ NODAL ID
                List<int> nodal_ID = new List<int>();
                for (int c = a; c < this.Offsets[o]; c++)
                {
                    nodal_ID.Add(this.Connectivity[c]);
                    a++;
                }
                //CREATE GEOMETRY BY TYPE
                switch (this.Types[o])
                {
                    case (int) VTK.Types.VTK_LINE:
                        this.lines.Add(new Line(this.Points[nodal_ID[0]],this.Points[nodal_ID[1]]));
                        break;
                    //case (int)Donkey.Model.Geometry.Types.VTK_TRIANGLE:
                    //    VTK_Triangle geox = new VTK_Triangle(nodal_ID, model);
                    //    model.Geometries.AppendData(geox, false);
                    //    break;
                    //TODO NEXT DATA TYPE
                    default:
                        break;
                }
            }
            return true;
        }

        //WRITE ELEMENTS - unused
        //private bool WriteElements(ReaderBuffer rb, ref ResultModel model)
        //{
        //    int count = rb.Element_ID.Max();
        //    //CREATE ELEMENTS
        //    DataList<Element> elements = new DataList<Element>();
        //    for (int i = 0; i <= count; i++)
        //    {
        //        elements.Add(new Element(ref model));
        //    }
        //    //ADD GEOMETRY TO THE ELEMENTS
        //    for (int i = 0; i < rb.Element_ID.Count; i++)
        //    {
        //        elements[rb.Element_ID[i]].Geometries_ID.Add(i);
        //    }
        //    //INITIALIZE ELEMENT DATA (CENTER)
        //    for (int i = 0; i <= count; i++)
        //    {
        //        elements[i].InitCenter();
        //    }
        //    model.SetElements(elements);
        //    return true;
        //}
        //WRITE RESULTING DATA

        //private bool ReadDisplacement(List<Vector3d> displacement)
        //{
        //    if (model.Nodes.Nodes == null || model.Nodes.Nodes.Count <= 1)
        //    {
        //        throw new ArgumentOutOfRangeException("Points read error");
        //    }
        //    if (displacement.Count == this.model.Nodes.Nodes.Count)
        //    {
        //        for (int i = 0; i < this.nl.Count; i++)
        //        {
        //            //this.pts[i].Value.Property.SetDisplacement(displacement[i]);
        //        }
        //        return true;
        //    }
        //    throw new ArgumentOutOfRangeException("Displacement is not same as points");
        //    //return false;
        //}
        #endregion

        #region UTILITY METHODS

        private string[] ReadArrString(string value)
        {
            char[] separators = new char[] { ' ', '\n' };
            return value.Trim().Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }

        private List<int> ReadArrInteger(string value)
        {
            string[] arrString = ReadArrString(value);
            List<int> arrInteger = new List<int>(arrString.Length);
            for (int i = 0; i < arrString.Length; i++)
            {
                arrInteger.Add(int.Parse(arrString[i]));
            }
            return arrInteger;
        }

        private List<double> ReadArrDouble(string value)
        {
            string[] arrString = ReadArrString(value);
            List<double> arrDouble = new List<double>(arrString.Length);
            for (int i = 0; i < arrString.Length; i++)
            {
                //SOME TIME FALSE (string correct format) - MS BUG?
                arrDouble.Add(double.Parse(arrString[i]));
            }
            return arrDouble;
        }

        private List<Vector3d> ReadArrVector(string value)
        {
            List<double> arrDouble = ReadArrDouble(value);
            List<Vector3d> arrVector = new List<Vector3d>(arrDouble.Count / 3);
            for (int i = 0; i < arrDouble.Count; i += 3)
            {
                arrVector.Add(new Vector3d(arrDouble[i], arrDouble[i + 1], arrDouble[i + 2]));
            }
            return arrVector;
        }
        #endregion
    }
}
