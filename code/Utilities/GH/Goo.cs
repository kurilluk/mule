﻿using System;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;

namespace Mule.Utilities.GH
{
    public abstract class Goo : IGH_Goo
    {
        public abstract bool IsValid { get; }

        public virtual string IsValidWhyNot
        {
            get
            {
                if (this.IsValid)
                    return string.Empty;
                return string.Format("This {0} is not valid, but I don't know why.", (object)this.TypeName);
            }
        }

        public abstract string TypeName { get; }

        public abstract string TypeDescription { get; }

        //protected GH_Goo()
        //{
        //}

        //protected GH_Goo(T internal_data)
        //{
        //  this.m_value = internal_data;
        //}

        //protected GH_Goo(GH_Goo<T> other)
        //{
        //  if (other == null)
        //    throw new ArgumentNullException("other");
        //  this.m_value = other.m_value;
        //}

        public abstract IGH_Goo Duplicate();

        public virtual IGH_GooProxy EmitProxy()
        {
            return (IGH_GooProxy)null;
        }

        public abstract override string ToString();

        public virtual bool CastFrom(object source)
        {
            return false;
        }

        public virtual bool CastTo<T>(out T target)
        {
            target = default(T);
            return false;
        }

        public virtual object ScriptVariable()
        {
            return (object)this;
        }

        public virtual bool Read(GH_IReader reader)
        {
            return true;
        }

        public virtual bool Write(GH_IWriter writer)
        {
            return true;
        }
    }
}
