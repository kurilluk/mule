﻿/*
 * Created by SharpDevelop.
 * User: kurilluk
 * Date: 04/04/2015
 * Time: 14:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Rhino.Geometry;
using Rhino.Display;
using Grasshopper.Kernel;


namespace Mule.Utilities.Preview
{
	/// <summary>
	/// Description of Arrow3D.
	/// </summary>
	public class Arrow3D
	{
		Brep b_arrow;
		Brep b_leg;
		readonly DisplayMaterial mat;
		public BoundingBox BB{ get; private set; }

		public Arrow3D(Line line)
		{
			CreateGeometry(line);
			this.mat = new DisplayMaterial();
			this.mat.Diffuse = System.Drawing.Color.Green;
		}

		private void CreateGeometry(Line line)
		{
			Vector3d direction = line.Direction;
			double height = line.Length * 0.25;
			double radius = height;
			var plane = new Plane(line.From, direction);
			Circle circle = new Rhino.Geometry.Circle(plane, radius * 0.15);
			Cylinder leg = new Rhino.Geometry.Cylinder(circle, line.Length - height);

			Vector3d revDirection = direction;
			revDirection.Reverse();
			var pl = new Plane(line.To, revDirection);
			Cone arrow = new Rhino.Geometry.Cone(pl, height, radius * 0.30);

			//CREATE BREP
			b_arrow = arrow.ToBrep(true);
			b_leg = leg.ToBrep(true, false);

			//CREATE BoundingBox
			this.BB = BoundingBox.Empty;
			this.BB.Union(b_arrow.GetBoundingBox(false));
			this.BB.Union(b_leg.GetBoundingBox(false));
		}

		public void Draw(IGH_PreviewArgs args)
		{
			args.Display.DrawBrepShaded(b_arrow, mat);
			args.Display.DrawBrepShaded(b_leg, mat);
		}
	}

}
