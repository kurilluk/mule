﻿/*
 * Created by SharpDevelop.
 * User: kuril
 * Date: 04/04/2015
 * Time: 16:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
 
using System;
using System.Drawing;
using Rhino.Geometry;
using Rhino.Display;
using Grasshopper.Kernel;

namespace Mule.Utilities.Preview
{
	/// <summary>
	/// Description of Branch3D.
	/// </summary>
  public class Branch3D
  {
    Brep b_arrow1;
    Brep b_arrow2;
    Brep b_leg;
    //Brep b_branch;
    Line l_arrow1;
    Line l_arrow2;
    Line l_leg;
    double size;
    Color _color;
    DisplayMaterial mat;
    public BoundingBox BB{get; private set;}

    public Branch3D(Line line, Color color, double size)
    {
    	this._color = color;
	    this.size = size;
	    CreateGeometry(line);
	    this.mat = new Rhino.Display.DisplayMaterial();
	    this.mat.Diffuse = Color.Blue;
    }

    private void CreateGeometry(Line line)
    {
      Vector3d direction = line.Direction;
      double height = line.Length * 0.35;
      double radius = size;

      Plane pl1 = new Plane(line.From, direction);
      Cone arrow1 = new Rhino.Geometry.Cone(pl1, height, radius * 0.15);
      l_arrow1 = new Line(pl1.Origin, direction, height * 0.25);

      Plane plane = new Plane((line.From + (pl1.ZAxis * height)), direction);
      Circle circle = new Rhino.Geometry.Circle(plane, radius * 0.15);
      Cylinder leg = new Rhino.Geometry.Cylinder(circle, line.Length - height * 2);
      l_leg = new Line((line.From + (pl1.ZAxis * height * 0.5)), plane.ZAxis, line.Length - height * 2 * 0.5);

      Vector3d revDirection = direction;
      revDirection.Reverse();
      Plane pl2 = new Plane(line.To, revDirection);
      Cone arrow2 = new Rhino.Geometry.Cone(pl2, height, radius * 0.15);
      l_arrow2 = new Line(pl2.Origin, revDirection, height * 0.25);

      //CREATE BREP
      //var breps = new List<Brep>();
      b_arrow1 = arrow1.ToBrep(false);
      b_leg = leg.ToBrep(false, false);
      b_arrow2 = arrow2.ToBrep(false);
      //breps.Add(b_arrow1);
      //breps.Add(b_leg);
      //breps.Add(b_arrow2);
      //b_branch = Brep.MergeBreps(breps, 0.1);

      //CREATE BoundingBox
      this.BB = BoundingBox.Empty;
      this.BB.Union(b_arrow1.GetBoundingBox(false));
      this.BB.Union(b_leg.GetBoundingBox(false));
      this.BB.Union(b_arrow2.GetBoundingBox(false));
      //this.BB.Union(b_branch.GetBoundingBox(false));
    }

    public void Draw(IGH_PreviewArgs args)
    {
      args.Display.DrawBrepShaded(b_arrow1, mat);
      args.Display.DrawBrepShaded(b_arrow2, mat);
      args.Display.DrawBrepShaded(b_leg, mat);
    }

    public void DrawWires(IGH_PreviewArgs args)
    {
      //      args.Display.DrawBrepWires(b_arrow1, Color.Blue);
      //      args.Display.DrawBrepWires(b_arrow2, Color.Blue);
      //      args.Display.DrawBrepWires(b_leg, Color.Blue);
      //args.Display.DrawBrepWires(b_branch, Color.Black);
      args.Display.DrawLine(l_arrow1, Color.Black, 2);
      args.Display.DrawLine(l_arrow2, Color.Black, 2);
      args.Display.DrawLine(l_leg, _color, (int) size);
    }
  }
}
