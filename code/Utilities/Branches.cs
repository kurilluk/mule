﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using System.Diagnostics;
using Rhino.Geometry.Intersect;
using Mule.Data;

namespace Mule.Utilities
{
    class Branches
    {
        static double tolerance = Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance;

        static public List<Curve> SplitBySize(List<Curve> curves, double size)
        {
            List<Curve> dividedCurves = new List<Curve>();
            foreach (Curve curve in curves)
            {
                double length = curve.GetLength();
                //TODO ADD TOLERANCE AND SPECIFIED ROAD SEGMENT SIZE
                if (length <= (1.5 * size)) // && length > 0.05) SOLVE PROBLEM WITH ARRAY? NO CURVE..
                {
                    dividedCurves.Add(curve);
                }
                else
                {
                    int count = Convert.ToInt16(Math.Round(length / size));
                    double[] divideParams = curve.DivideByCount(count, false);
                    Curve[] dividedCurve = curve.Split(divideParams);
                    dividedCurves.AddRange(dividedCurve);
                }
            }
            return dividedCurves;
        }

        static public List<Branch> SplitIntersectedCurves(List<Branch> C) { return SplitIntersectedCurves(C, new List<Point3d>()); }
        static public List<Branch> SplitIntersectedCurves(List<Branch> C, List<Point3d> pts)
        {
            if (C == null || C.Count == 0)
            {
                return null;
            }

            List<Branch> newBranches = new List<Branch>();

            List<List<double>> cutParams = new List<List<double>>(C.Count);
            for (int i = 0; i < C.Count; i++)
            {
                cutParams.Add(new List<double>());
                CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveSelf(C[i].Crv, tolerance);
                if ((intersections != null) && (intersections.Count > 0))
                {
                    for (int k = 0; k < intersections.Count; k++)
                    {
                        cutParams[i].Add(intersections[k].ParameterA);
                        cutParams[i].Add(intersections[k].ParameterB);
                    }
                }

                if(pts != null || pts.Count > 0)
                    for (int p = 0; p < pts.Count; p++)
                    {
                        double param;
                        if (C[i].Crv.ClosestPoint(pts[p], out param, tolerance))
                            cutParams[i].Add(param);
                    }
            }

            for (int i = 0; i < C.Count; i++)
            {
                for (int j = i + 1; j < C.Count; j++)
                {
                    CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveCurve(C[i].Crv, C[j].Crv, 0.1, 1);
                    if ((intersections != null) && (intersections.Count > 0))
                    {
                        for (int k = 0; k < intersections.Count; k++)
                        {
                            cutParams[i].Add(intersections[k].ParameterA);
                            cutParams[j].Add(intersections[k].ParameterB);
                        }
                    }
                }
            }

            //split the branches at their cut params and add the new branchces to newBranches
            for (int i = 0; i < C.Count; i++)
            {
                newBranches.AddRange(C[i].Split(cutParams[i]));
            }
            return newBranches;
        }

        private static bool isDividable(Curve c, double param)
        {
            return param > c.Domain.Min && param < c.Domain.Max;
        }

        private static List<Curve> divide(Curve c, double param)
        {
            List<Curve> newCrvs = new List<Curve>();
            if (!isDividable(c, param))
            {
                newCrvs.Add(c);
                return newCrvs;
            }
            Curve[] crvs = c.Split(param);
            newCrvs.AddRange(crvs);
            return newCrvs;
        }
    }
}
