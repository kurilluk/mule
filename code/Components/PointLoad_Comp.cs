﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Mule.Data;


namespace Mule.Components
{
    public class PointLoad_Comp : GH_Component
     {
        public PointLoad_Comp()
            : base("PointLoad", "Load", "Point load [N]"
            , "Donkey", "Mule")
        {  }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Point", "P", "Position", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddVectorParameter("Direction", "V", "Force direction", GH_ParamAccess.item,new Vector3d(0,0,-1));
            pManager[1].Optional = true;
            pManager.HideParameter(1);
            pManager.AddNumberParameter("Magnitude", "F", "Force mangitude [N]", GH_ParamAccess.item, 10);
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Loads_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            Point3d node = Point3d.Unset;
            bool m_node = DA.GetData("Point",ref node);

            Vector3d vector = Vector3d.Unset;
            bool m_vector = DA.GetData("Direction",ref vector);
            if(!vector.IsUnitVector)
                vector.Unitize();

            double magnitude = 10;
            bool m_force = DA.GetData("Magnitude", ref magnitude);

            PointLoad pt_load = new PointLoad(node, vector*magnitude);

            DA.SetData(0, pt_load);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{E6D476B3-5F9E-461C-902A-04468298178A}"); }
        }

        protected override Bitmap Icon
        {
            get { return Properties.Resources.mule_load_nodal; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }
    }
}
