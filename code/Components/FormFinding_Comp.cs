﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using Mule.Utilities.VTK;

namespace Mule.Components{
    public class FormFinding_Comp: GH_Component
    {
        #region FIELD
        internal List<Line> m_model = new List<Line>();
        #endregion

        #region CONSTRUCTOR
        public FormFinding_Comp()
            : base("FormFinding",
            "FormFinding",
            "Funicular form finding component (alpha)",
            "Donkey", "Mule")
        {
            this.m_model = (List<Line>)null;
        }
        #endregion

        #region SOLVER
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("o","o","o",GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddLineParameter("F", "Form", "Founded form", GH_ParamAccess.list);
            pManager.HideParameter(0);
            //pManager.AddParameter(new AnalyticalModel_Param());
        }

        //SOLVER
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool run = false;
            DA.GetData(0, ref run);

            if (run)
                if (!RunFormFinding())
                {
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Form-finding false!");
                    this.m_model = (List<Line>)null;
                    DA.AbortComponentSolution();
                    return;
                }

            VTU_Reader reader = new VTU_Reader();
            List<Line> shape = reader.Read("C:\\_test\\mule.topology.vtu");

            this.m_model = shape;

            DA.SetDataList(0, shape);
        }

        public override void ClearData()
        {
            this.m_model = (List<Line>)null;
            base.ClearData();
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null)
            {
                foreach (Line line in m_model)
                {
                    args.Display.DrawLine(line, Color.White, 2);
                }
                ////ResultModel model = new ResultModel(m_model);
                //m_model.DrawGeometryColour(args);
                //////if (this.m_previewValue)
                //////{
                ////    args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
                ////    m_model.DrawElementValue(args);
                ////    args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
                //////}
            }
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = base.ClippingBox;
                if(m_model != null)
                    foreach (Line line in m_model)
                        bb.Union(line.BoundingBox);

                return bb;
            }
        }
        #endregion

        #region COMPONENT SETUP

        public override Guid ComponentGuid
        {
            get { return new Guid("{B676DDEC-4D38-44DC-8DC2-E218DF00944E}"); }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_form_finding; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quinary; } //.quinary; }
        }
        #endregion

        #region METHODS
        //RUN FORM-FINDING
        private bool RunFormFinding()
        {
            Rhino.Runtime.HostUtils.DisplayOleAlerts(false);

            //CREATE ANALYSIS COMAND AND RUN ANALYSIS AS PROCESS
            Process process = new Process();
            ProcessStartInfo start = process.StartInfo;
            start.FileName = "python.exe";
            start.RedirectStandardOutput = false;  // change this to read data to C# (also uncoment data reciver and read line)
            start.CreateNoWindow = true;
            start.UseShellExecute = false; // make sure we can&nbsp;read the output from stdout
            start.Arguments = "D:\\Cloud\\backup_Dropbox\\BRG\\Project\\ForceDencity\\src\\mule.py"; // start the python program with two parameters

            process.Start();
            process.WaitForExit();
            if (process.ExitCode == 0)
                return true;
            else
                return false;
        }
        #endregion
    }
}
