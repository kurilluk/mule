﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Attributes;
using Rhino;
using Rhino.Runtime;
using Rhino.Geometry;
using Rhino.Collections;
using Rhino.DocObjects;
using Mule.Data;

namespace Mule.Components{
    public class Branch_Comp : GH_Component
    {
        #region FIELD
        private bool as_force_density;
        private bool fixed_q;
        #endregion

        #region COMPONENT SETUP
        /// <summary>
        /// Initializes a new instance of the ElementBeam_Comp class.
        /// </summary>
        public Branch_Comp()
            : base("Branch", "B",
                "Create branches with custom force dencity value",
                "Donkey", "Mule")
        {
            this.NickName = this.Name;
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_branch_1; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{B066779F-9D26-4DCA-88E6-CB53B9763F26}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }
        #endregion

        #region SOLVER
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddCurveParameter("Geometry", "G", "Set curve as branch", GH_ParamAccess.item); //TODO MElement
            pManager.HideParameter(0);  //hide preview
            pManager.AddNumberParameter("Force density", "q", "Set force dencity value", GH_ParamAccess.item, 1.0);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Branch_Param());
        }

        protected override void BeforeSolveInstance()
        {
            if (as_force_density)
            {
                Params.Input[1].Name = "Force density";
                Params.Input[1].NickName = "q";
                Params.Input[1].Description = "Set force dencity value";
            }
            else
            {
                Params.Input[1].Name = "Force";
                Params.Input[1].NickName = "F";
                Params.Input[1].Description = "Set force magnitude [N]";
            }

            if (fixed_q && as_force_density)
                this.Message = "FIXED";
            else
                this.Message = "";
            base.BeforeSolveInstance();
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_Curve crv = new GH_Curve();

            if (!DA.GetData("Geometry", ref crv))
            {
                DA.AbortComponentSolution();
                return;
            }

            double force = 1.0;
            if (!DA.GetData(1 , ref force))
            {
                DA.AbortComponentSolution();
                return;
            }

            if (crv.IsReferencedGeometry)
            {
                RhinoObject item = RhinoDoc.ActiveDoc.Objects.Find(crv.ReferenceID);
                string name = item.Name;

                if (!String.IsNullOrEmpty(name))
                {
                    try
                    {
                        double result = Convert.ToDouble(name);
                        force = result;

                        this.AddRuntimeMessage(GH_RuntimeMessageLevel.Remark, " [" + (this.RunCount-1) + "] Reading referenced object force value: " + name);
                    }
                    catch (FormatException)
                    {
                        this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Unable to convert " + name + " to a Double.");
                    }
                    catch (OverflowException)
                    {
                        //Console.WriteLine("'{0}' is outside the range of a Double.", name);
                    }
                }
            }

            Branch branch = new Branch(crv.Value, force, as_force_density, (fixed_q && as_force_density));
            DA.SetData(0, branch);

            //var aaa = Rhino.RhinoDoc.ActiveDoc.Objects.GetSelectedObjects(false, false);

            //IEnumerator<Rhino.DocObjects.RhinoObject> enumerator = aaa.GetEnumerator();
            //while (enumerator.MoveNext())
            //{
            //    Rhino.DocObjects.RhinoObject item = enumerator.Current;
            //    this.Message = item.Name;
            //}


            //obj = new Rhino.DocObjects.RhinoObject(

            //Rhino.DocObjects.RhinoObject object = Rhino.RhinoDoc.ActiveDoc.Objects.Find(Guid.NewGuid());
            
        }
        #endregion

        #region MENU

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Force magnitude [N]", new EventHandler(this.Change_force_input), true, !this.as_force_density);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Force density (q)", new EventHandler(this.Change_force_input), true, this.as_force_density);
            GH_ActiveObject.Menu_AppendSeparator((ToolStrip)menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Fixed force density value", new EventHandler(this.Fixed_q), true, this.fixed_q)
                .ToolTipText = "Force density value will not change" + Environment.NewLine + "by subdividing or spliting branches.";
        }

        private void Fixed_q(object sender, EventArgs e)
        {
            this.RecordUndoEvent("Fixed_q");
            this.fixed_q = !this.fixed_q;
            if(as_force_density)
                this.ExpireSolution(true);
        }

        private void Change_force_input(object sender, EventArgs e)
        {
            this.RecordUndoEvent("Force_change");
            this.as_force_density = !this.as_force_density;
            this.ExpireSolution(true);
        }


        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("force_type", this.as_force_density);
            writer.SetBoolean("fixed_q", this.fixed_q);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.as_force_density = reader.GetBoolean("force_type");
            this.fixed_q = reader.GetBoolean("fixed_q");
            return base.Read(reader);
        }

        #endregion

    }
}