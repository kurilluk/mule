﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Mule.Data;
using Rhino.Geometry;

namespace Mule.Components
{
    public class DynamicRelaxation_Comp : GH_Component
    {
        public DynamicRelaxation_Comp()
            : base("DynamicRelaxation", "DR", "Dynamic relaxation"
            , "Donkey", "Mule")
        {  }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Topology_Param()); //,"T","T","T",GH_ParamAccess.item);
            pManager.AddIntegerParameter("i", "i", "Number of iterations", GH_ParamAccess.item);
            pManager.AddNumberParameter("dt", "dt", "dumping", GH_ParamAccess.item);
            pManager.AddNumberParameter("m", "m", "mass", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("P", "P", "free nodes", GH_ParamAccess.list);
            pManager.AddVectorParameter("V", "V", "recitual forces", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            Topology topology = new Topology();
            if (!DA.GetData(0, ref topology))
                return;

            int steps = 10;
            DA.GetData(1, ref steps);


            double dt = 1.0;
            DA.GetData(2, ref dt);

            double mass = 1.0;
            DA.GetData(3, ref mass);

            //Topology topology = topology_goo.Value;

            //List<double[]> m = new List<double[]>(topology.Nodes.Count);
            double[,] xyz = new double[topology.Nodes.Count,3];

            //Point3d[] pts = topology.Nodes.Keys.ToArray();
            int i = 0;
            foreach (Point3d pt in topology.Nodes.Keys)
            //m.Add(new double[]{pt.X,pt.Y,pt.Z});
            {
                xyz[i,0] = pt.X;
                xyz[i,1] = pt.Y;
                xyz[i,2] = pt.Z;
                i++;
            }

            List<int> free = new List<int>();
            List<int> fix = new List<int>();
            int id = 0;
            foreach (bool status in topology.Fixed)
            //m.Add(new double[]{pt.X,pt.Y,pt.Z});
            {
                if (status)
                    fix.Add(id);
                else
                    free.Add(id);
                id++;
            }

            //double[][] p = new double[topology.Nodes.Count][];
            double[,] p = new double[topology.Forces.Count,3];
            i = 0;
            foreach (Point3d p_vec in topology.Forces)
            {
                //p[i] = new double[] { p_vec.X, p_vec.Y, p_vec.Z };
                p[i,0] = p_vec.X;
                p[i,1] = p_vec.Y;
                p[i,2] = p_vec.Z;
                i++;
            }

            i = 0;
            int[,] edges = new int[topology.Branches.Count,2];
            double[] q = new double[topology.Branches.Count];
            foreach (Branch branch in topology.Branches)
            {
                edges[i,0] = topology.Nodes[branch.Crv.PointAtStart];
                edges[i,1] = topology.Nodes[branch.Crv.PointAtEnd];
                q[i] = branch.q;
                i++;
            }

           double[,] resitual_forces;
           double[,] free_xyz = Solver.ForceDensity.DynamicRelaxation(out resitual_forces, xyz,
               free.ToArray(), fix.ToArray(), p, edges, q, steps, dt, mass);

           List<Point3d> pts = new List<Point3d>();
           for (int n = 0; n < free_xyz.GetLength(0); n++)
           {
                pts.Add(new Point3d(free_xyz[n,0],free_xyz[n,1],free_xyz[n,2]));
           }

           DA.SetDataList(0, pts);

           List<Vector3d> vecs = new List<Vector3d>();
           for (int n = 0; n < resitual_forces.GetLength(0); n++)
           {
               vecs.Add(new Vector3d(resitual_forces[n, 0], resitual_forces[n, 1], resitual_forces[n, 2]));
           }

           DA.SetDataList(1, vecs);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{007B8DEA-789F-4AB2-B14A-AA975D388986}"); }
        }

        protected override Bitmap Icon
        {
            get { return Properties.Resources.mule_form_finding; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

    }
}
