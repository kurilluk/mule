﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Windows.Forms;
using Mule.Data;

namespace Mule.Components
{
    public class Splitting_Comp : GH_Component
    {
        #region COMPONENT SETUP

        public Splitting_Comp()
            : base("Split Branchces", "Splitting",
                "Splitting intersecting branchces",
                "Donkey", "Mule")
        {
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_spliting_branches; }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{A93E3B01-9AF4-4057-B8BF-E5A248F5CC15}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        #endregion

        #region SOLVER
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Branch_Param());
            pManager.AddPointParameter("Point", "P", "Points to split curves", GH_ParamAccess.list);
            pManager[1].Optional = true;
            //pManager.AddNumberParameter("Length", "L", "Segment length [mm]", GH_ParamAccess.item); //+Space.Units, GH_ParamAccess.item);
            //pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Branch_Param());
            //pManager.AddCurveParameter("Segments", "S", "Splitted segments", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<Branch> branches = new List<Branch>();
            if (!DA.GetDataList<Branch>("Branches", branches))
            {
                DA.AbortComponentSolution();
                return;
            }

            List<Branch> outputBranches;

            List<Point3d> splittingPoints = new List<Point3d>();
            if(DA.GetDataList<Point3d>(1, splittingPoints))
                outputBranches = Utilities.Branches.SplitIntersectedCurves(branches, splittingPoints);
            else
                outputBranches = Utilities.Branches.SplitIntersectedCurves(branches);

            DA.SetDataList(0, outputBranches);
        }
        #endregion

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {

            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendWarningsAndErrors(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            //this.AppendAdditionalMenuItems(menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }
        #endregion
    }
}