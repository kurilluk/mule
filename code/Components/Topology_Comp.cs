﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using Rhino.Geometry;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Gradient;
using Mule.Data;
using Mule.Utilities.Preview;
using Mule.Utilities.VTK;

namespace Mule.Components{
    public sealed class Topology_Comp : GH_Component
    {
        #region FIELD
        bool m_gravitation;
        Topology m_topology;
        #endregion

        #region COMPONENT SETUP
        /// <summary>
        /// Initializes a new instance of the ElementBeam_Comp class.
        /// </summary>
        public Topology_Comp()
            : base("Topology", "T",
                "Create connection map with boundary condictions",
                "Donkey", "Mule")
        {
            this.m_gravitation = true;
            this.NickName = this.Name;
            this.m_topology = (Topology)null;
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.mule_topology; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{D7ECBFC7-DB7D-4DF5-9865-A81A7ED23BC2}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }
        #endregion

        #region SOLVER
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Branch_Param());
            pManager.HideParameter(0);  //hide preview
            pManager.AddPointParameter("Fixed nodes", "Nf", "Fixed nodes", GH_ParamAccess.list);  //HACK think about boundary condictions (read also loads) 
            pManager.AddParameter(new Loads_Param());
            //pManager.AddPointParameter("Loads", "p", "Point loads", GH_ParamAccess.list);
            pManager[2].Optional = true;
            pManager.AddIntegerParameter("Sub-divide", "d", "Subdivide branch by count", GH_ParamAccess.item, 5);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("P", "P", "nodes position", GH_ParamAccess.list);
            pManager.AddNumberParameter("ID", "ID", "nodes id", GH_ParamAccess.list);
            pManager.HideParameter(0);
            pManager.AddBooleanParameter("o", "o", "o", GH_ParamAccess.item);
            pManager.AddParameter(new Topology_Param());
            //pManager.AddParameter(new T_Element_Param());
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            var branches = new List<Branch>();
            if (!DA.GetDataList<Branch>("Branches", branches))
            {
                DA.AbortComponentSolution();
                return;
            }

            var fix_pts = new List<Point3d>();
            if (!DA.GetDataList<Point3d>("Fixed nodes", fix_pts))
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No fixed nodes are defined!");
                DA.AbortComponentSolution();
                return;
            }

            var pt_loads = new List<PointLoad>();
            DA.GetDataList<PointLoad>("Loads", pt_loads);

            int sub_divide = 0;
            DA.GetData<int>("Sub-divide", ref sub_divide);


            //TOPOLOGY
            var topology = new Topology(branches);
            try
            {
                topology.SetNodeData(fix_pts, pt_loads);
            }
            catch(ArgumentException e)
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message);
            }

            if(sub_divide > 0)
                topology.SubdivideBranches(sub_divide+1);

            if (m_gravitation)
                topology.AddGravitation(0.1);  //TODO can be a parameter
  

            //WRITE DATA
            var file = new VTU_Writer();
            file.Write(topology, "C:\\_test\\test.topology.vtu"); //HACK if folder doesn't exist, create it...


            var ids = new List<int>();
            var pts = new List<Point3d>();
            foreach (Branch branch in topology.Branches)
            {
                ids.Add(topology.Nodes[branch.Crv.PointAtStart]);
                pts.Add(branch.Crv.PointAtStart);
                ids.Add(topology.Nodes[branch.Crv.PointAtEnd]);
                pts.Add(branch.Crv.PointAtEnd);
            }

            DA.SetDataList(0, pts);
            DA.SetDataList(1, ids);
            DA.SetData("o", true);

            Vector3d sum = Vector3d.Zero;
            foreach (Vector3d vec in topology.Forces)
            {
                sum += vec;
            }
            this.AddRuntimeMessage(GH_RuntimeMessageLevel.Remark, "Sum of external forces: " + sum);

            topology.GeneratePreview();
            this.m_topology = topology;
            DA.SetData(3,topology);
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_topology != null)
            {
                foreach (Branch3D branch in m_topology.Branches_preview)
                	branch.DrawWires(args);
                    //i++;
            }
        }
        
        public override void DrawViewportMeshes(IGH_PreviewArgs args)
        {
			if (m_topology != null)
           	{
				foreach(Mule.Utilities.Preview.Arrow3D arrow in m_topology.Loads)
					arrow.Draw(args);				
			}
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = base.ClippingBox;
                if (m_topology != null)
                {
                    foreach (Branch branch in m_topology.Branches)
                        bb.Union(branch.Crv.GetBoundingBox(false));
                   	foreach (Mule.Utilities.Preview.Arrow3D arrow in m_topology.Loads)
                        bb.Union(arrow.BB);
                }

                return bb;
            }
        }

        public override void ClearData()
        {
            this.m_topology = (Topology)null;
            base.ClearData();
        }
        #endregion

        #region MENU

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Gravitation", new EventHandler(this.Gravity_Click), true, this.m_gravitation)
            .ToolTipText = "Turn global gravitation on/off.";
            //Menu_AppendSeparator(menu);
        }

        private void Gravity_Click(object sender, EventArgs e)
        {
            this.RecordUndoEvent("Gravitation");
            this.m_gravitation = !this.m_gravitation;
            this.ExpireSolution(true);
        }


        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("gravitation", this.m_gravitation);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_gravitation = reader.GetBoolean("gravitation");
            return base.Read(reader);
        }

        #endregion

        #region METHODS
        #endregion
    }
}