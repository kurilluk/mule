﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Math.Decompositions;


namespace Solver
{

    public class ForceDensity_App
    {
        static void Main(string[] args)
        {
            Calculate();

        }

        static void Print(object o)
        {
            Console.Out.WriteLine(o);
        }

        static void PrintMatrix(double[,] o, string name)
        {
            Console.Out.WriteLine("\n" + name);
            Console.Out.WriteLine(o.ToString(OctaveMatrixFormatProvider.InvariantCulture));
        }

        public static string Hi()
        {
            return "Hi, is it anybody out there?";
        }

        public static void Calculate()
        {
            int[,] edges = {{0, 1}, {1, 2}, {2, 3}, {3, 4}};
            double[,] vertices = { { 0, 0, 5.0 }, { 5.0, 0, 0 }, { 10.0, 0, 0 }, { 2.5, 0, 1 }, { 7.5, 0, 1 } };
            double[,] p = { { 0, 0, 0 }, { 0, 0, 10 }, { 0, 0, 10 }, { 0, 0, 10 }, { 0, 0, 0} };

            //int[,] edges = { { 0, 4 }, { 1, 4 }, { 2, 4 }, { 3, 4 } };
            Print("Edges:");
            Print(Matrix.ToString(edges.ToDouble(), OctaveMatrixFormatProvider.CurrentCulture));
            int m = edges.GetLength(0);
            Print("m:");
            Print(m);

            var data = Matrix.Vector<double>(m, -1).Concatenate<double>(Matrix.Vector<double>(m, 1));
            Print("Data:");
            Print(data.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var cols = edges.Reshape();
            Print("Cols:");
            Print(cols.ToDouble().ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var rows = Matrix.Indices(0, m).Concatenate<int>(Matrix.Indices(0, m));
            //var rows = Matrix.Interval(from: 0, to: m).Concatenate<double>(Matrix.Interval<double>(from: 0.0, to: (double)m));
            Print("Rows:");
            Print(rows.ToDouble().ToString(OctaveMatrixFormatProvider.InvariantCulture));
            
            var C = Matrix.Create<double>(m, m + 1, 0);
            for (int i = 0; i < data.Length; i++)
                C.SetValue(data[i], rows[i], cols[i]);

            Console.Out.WriteLine("\nC:");
            Console.Out.WriteLine(C.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            int[] free = {1,2,3}; //TODO read id from 0, 1
            var Ci = C.Transpose().Submatrix(free).Transpose(); //TODO find submatrix for column not only row!!!
            Console.Out.WriteLine("\nCi:");
            Console.Out.WriteLine(Ci.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var Cit = Ci.Transpose();
            Console.Out.WriteLine("\nCit:");
            Console.Out.WriteLine(Cit.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            int[] fix = {0, 4}; //TODO read id from 0, 1
            var Cf = C.Transpose().Submatrix(fix).Transpose();
            Console.Out.WriteLine("\nCf:");
            Console.Out.WriteLine(Cf.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var xyz = vertices;
            Console.Out.WriteLine("\nxyz:");
            Console.Out.WriteLine(xyz.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var xyz_fix = xyz.Submatrix(fix);
            Console.Out.WriteLine("\nxyz_fix:");
            Console.Out.WriteLine(xyz_fix.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var p_free = p.Submatrix(free);
            Console.Out.WriteLine("\np_free:");
            Console.Out.WriteLine(p_free.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            //var q = Matrix.Create<double>(m, 1, 1.0); //.Reshape<double>();
            //Console.Out.WriteLine("\nq:");
            //Console.Out.WriteLine(q.ToString(OctaveMatrixFormatProvider.InvariantCulture));
            var q = new double[] { 1, 1, 1, 1 }; //.Reshape<double>();

            //SingularValueDecomposition svd = new SingularValueDecomposition(q);
            //var Q = svd.DiagonalMatrix;
            var Q = Matrix.Diagonal<double>(m, q);
            Console.Out.WriteLine("\nQ:");
            Console.Out.WriteLine(Q.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var A = Cit.Multiply(Q).Multiply(Ci);
            Console.Out.WriteLine("\nA:");
            Console.Out.WriteLine(A.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var b = p_free.Subtract(Cit.Multiply(Q.Multiply(Cf.Multiply(xyz_fix))));
            Console.Out.WriteLine("\nb:");
            Console.Out.WriteLine(b.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var x_xyz_free = A.Solve(b);
            Console.Out.WriteLine("\nr_xyz_free:");
            Console.Out.WriteLine(x_xyz_free.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            ///
            PrintMatrix(xyz_fix, "xyz_free");

            var Cf_xyz = Cf.Multiply(xyz_fix);
            Console.Out.WriteLine("\nCf_xyz:");
            Console.Out.WriteLine(Cf_xyz.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var Q_Cf_xyz = Q.Multiply(Cf.Multiply(xyz_fix));
            PrintMatrix(Q_Cf_xyz, "Q_Cf_xyz:");

            var Cit_Q_Cf_xyz = Cit.Multiply(Q.Multiply(Cf.Multiply(xyz_fix)));
            PrintMatrix(Cit_Q_Cf_xyz, "Cit_Q_Cf_xyz:");

            var p_Cit_Q_Cf_xyz = p_free.Subtract(Cit_Q_Cf_xyz);
            PrintMatrix(p_Cit_Q_Cf_xyz, "p_free-Cit_Q_Cf_xyz:");

            ///
            //Test
            var forces = p_free.Subtract(A.Multiply(x_xyz_free));
            PrintMatrix(forces, "RForces:");

            //CholeskyDecomposition chol = new CholeskyDecomposition(A);
            //var R = chol.LeftTriangularFactor;

            Console.In.ReadLine();
        }

        static void Test()
        {
            double[] v = { 4, 5, 6 };
            int[,] edges = { { 0, 4 }, { 1, 4 }, { 2, 4 }, { 3, 4 } };
            int m = edges.GetLength(0);
            Console.Out.WriteLine(m);

            var data = Matrix.Vector<double>(m, -1).Concatenate<double>(Matrix.Vector<double>(m, 1));
            Print(data.ToString(OctaveMatrixFormatProvider.InvariantCulture));

            var rows = Matrix.Indices(0, m).Concatenate<int>(Matrix.Indices(0, m));
            //var rows = Matrix.Interval(from: 0, to: m).Concatenate<double>(Matrix.Interval<double>(from: 0.0, to: (double)m));
            Print(rows.ToDouble().ToString(OctaveMatrixFormatProvider.InvariantCulture));


            var A = Matrix.Create<double>(m, m + 1, 0);
            //var B = A.Add(new double[] {1,2,3},0);
            //var C = A.Add(3);

            var cols = edges.Reshape();

            for (int i = 0; i < data.Length; i++)
                A.SetValue(data[i], rows[i], cols[i]);



            //var X = Matrix.InsertColumn(A,


            var result = v.Multiply(2);
            double[] a = v.ElementwiseMultiply(v); // v .* 2: { 8,  10,  12 }
            //double[] b = v.ElementwiseDivide(2);   // v ./ 2: { 2,  2.5,  3 }
            //double[] c = v.ElementwisePower(2);    // v .^ 2: { 16,  25, 36 }

            Console.Out.WriteLine("A:");
            Console.Out.WriteLine(A.ToString(OctaveMatrixFormatProvider.InvariantCulture));
            //Console.Out.WriteLine("B:");
            //Console.Out.WriteLine(B.ToString(OctaveMatrixFormatProvider.InvariantCulture));
            //Console.Out.WriteLine("C:");
            //Console.Out.WriteLine(C.ToString(OctaveMatrixFormatProvider.InvariantCulture));
            Console.Out.WriteLine("Else:");
            //Console.Out.WriteLine(cols.ToString(OctaveMatrixFormatProvider.InvariantCulture));
            Console.Out.WriteLine(Matrix.ToString(edges.ToDouble(), OctaveMatrixFormatProvider.CurrentCulture));
            Console.In.ReadLine();
        }
    }
}
