﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Math.Decompositions;

namespace Mule.Solver
{
    public static class ForceDensity
    {
        public static double[,] Calculate(double[,] xyz, int[] free, int[] fix, double[,] p, int[,] edges, double[] q)
        {
            int m = edges.GetLength(0);
            var data = Matrix.Vector<double>(m, -1).Concatenate<double>(Matrix.Vector<double>(m, 1));
            var cols = edges.Reshape();
            var rows = Matrix.Indices(0, m).Concatenate<int>(Matrix.Indices(0, m));

            var C = Matrix.Create<double>(m, m + 1, 0);
            for (int i = 0; i < data.Length; i++)
                C.SetValue(data[i], rows[i], cols[i]);

            //free = new int[] { 1, 2, 3 }; //TODO read id from 0, 1
            var Ci = C.Transpose().Submatrix(free).Transpose(); //?TODO find submatrix for column not only row!!!
            var Cit = Ci.Transpose();

            //fix = new int[] { 0, 4 }; //TODO read id from 0, 1
            var Cf = C.Transpose().Submatrix(fix).Transpose();
            
            var xyz_fix = xyz.Submatrix(fix);
            var p_free = p.Submatrix(free);

            //q = new double[] { 1, 1, 1, 1 }; //.Reshape<double>(); //TODO change!
            var Q = Matrix.Diagonal<double>(m, q);

            var A = Cit.Multiply(Q).Multiply(Ci);
            var b = p_free.Subtract(Cit.Multiply(Q.Multiply(Cf.Multiply(xyz_fix))));
            var xyz_free = A.Solve(b); //return only free nodes
            return Combine(xyz, xyz_free, free);

        }

        public static double[,] RecitualForces(double[,] xyz, int[] free, int[] fix, double[,] p, int[,] edges, double[] q)
        {
            var r = Matrix.Create(xyz.GetLength(0), 3, 0.0);
            int m = edges.GetLength(0);
            var data = Matrix.Vector<double>(m, -1).Concatenate<double>(Matrix.Vector<double>(m, 1));
            var cols = edges.Reshape();
            var rows = Matrix.Indices(0, m).Concatenate<int>(Matrix.Indices(0, m));

            var C = Matrix.Create<double>(m, m + 1, 0);
            for (int i = 0; i < data.Length; i++)
                C.SetValue(data[i], rows[i], cols[i]);

            var Ct = C.Transpose();

            var Q = Matrix.Diagonal<double>(m, q);

            var T = Ct.Multiply(Q).Multiply(C).Multiply(xyz);
            //return T;
            var r_free = T.Subtract(p);
            return Combine(r, r_free, free);
            //var b = p_free.Subtract(Cit.Multiply(Q.Multiply(Cf.Multiply(xyz_fix))));
            //return A.Solve(b); //return only free nodes

        }

        public static double[,] DynamicRelaxation(out double[,] r, double[,] xyz, int[] free, int[] fix, double[,] p, int[,] edges, double[] q, int steps, double dt, double mass)
        {
            int m = edges.GetLength(0);
            var data = Matrix.Vector<double>(m, -1).Concatenate<double>(Matrix.Vector<double>(m, 1));
            var cols = edges.Reshape();
            var rows = Matrix.Indices(0, m).Concatenate<int>(Matrix.Indices(0, m));

            var C = Matrix.Create<double>(m, m + 1, 0);
            for (int i = 0; i < data.Length; i++)
                C.SetValue(data[i], rows[i], cols[i]);

            var Ci = C.Transpose().Submatrix(free).Transpose(); //?TODO find submatrix for column not only row!!!
            var Cit = Ci.Transpose();

            var Ct = C.Transpose();

            var Q = Matrix.Diagonal<double>(m, q);

            var xyz_free = xyz.Submatrix(free);
            var xyz_fix = xyz.Submatrix(fix);
            var p_free = p.Submatrix(free);
            //var aaa = Matrix.Create(m, 0.0);
            //aaa.Add(xyz,

            //xyz[fix] = xyz_fix;

            var v = Matrix.Create(xyz.GetLength(0), 3, 0.0);
            r = Matrix.Create(xyz.GetLength(0), 3, 0.0);
            var r_free = r.Submatrix(free);
            //int steps = 2;
            double step = 0.1;
            //double dt = 1.0;
            //double mass = 1.0;

            var v_free = v.Submatrix(free);

            for (int i = 0; i < steps; i++)
            {
                double[,] xyz_tem = (double[,])xyz.Clone();
                double[,] v_temp = (double[,])v.Clone();
                double[,] dx = v_temp.Multiply(dt);
                xyz_free = xyz_tem.Submatrix(free).Add(dx.Submatrix(free));
                //double[,] r_free = r.Submatrix(free);
                xyz = Combine(xyz, xyz_free, free);
                double[,] T = Cit.Multiply(Q).Multiply(C).Multiply(xyz);
                r_free = p_free.Subtract(T);
                var a = r_free.Multiply(step).Divide(mass);
                var dv = dt.Multiply(a);
                v_free = v_temp.Submatrix(free).Add(dv);
                v = Combine(v, v_free, free);
                dx = v.Multiply(dt);
                xyz_free = xyz_tem.Submatrix(free).Add(dx.Submatrix(free));
                xyz = Combine(xyz, xyz_free, free);
            }
            r = Combine(r, r_free, free);
            return xyz;
        }

        static private double[,] Combine(double[,] A, double[,] row, int[] indexes)
        {
            int i = 0;
            foreach (int index in indexes)
            {
                A.RemoveRow(index);
                A.SetRow(index, row.GetRow(i));
                i++;
            }
            return A;
        }
    }
}
